const mongoose = require('mongoose');

const BookSchema = mongoose.Schema({
  user_id: {type: mongoose.Schema.Types.ObjectId, unique: true},
  project_id: String,
  publisher_id: String,
  zip_name: String,
  date_updated: Date,
  is_finished: {type: Boolean, default: false},
},
{
  collection: 'books',
  autoIndex: true,
});

module.exports = mongoose.model('Book', BookSchema); 
