const mongoose = require('mongoose');

const buildIndexParamsDefault = require('./buildIndexParamsDefault');

const UserSettingsSchema = mongoose.Schema({
  user_id: {type: mongoose.Schema.ObjectId, index: {unique: true}},
  dark_theme: {type: Boolean, default: false},
  font_scaling: Number,
  build_index_params: {
    type: mongoose.Schema.Types.Mixed,
    default: buildIndexParamsDefault,
  },
  concordance_lines: {
    top: {type: Number, min: 0, max: 10, default: 1},
    bottom: {type: Number, min: 0, max: 10, default: 1},
  },
  date_updated: Date,
}, {
  collection: 'user_settings',
  autoIndex: true,
});

module.exports = mongoose.model('UserSettings', UserSettingsSchema);
