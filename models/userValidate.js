const isEmail = require('validator').isEmail;
const trim = require('validator').trim;

const config = require('../conf/config');
const regexps = require('../lib/constants-regexp');

// Validation error messages
const ERROR_ALL_FIELDS_REQUIRED = 'Please fill in all required fields.';
const ERROR_PASSWORD_TOO_SHORT_8 =
  'Password should contain 8 or more symbols.';
const ERROR_EMAIL_NOT_VALID = 'Please enter valid email address.';
const ERROR_LIMITED_DOMAINS_ONLY =
  'Registration is limited to specific domains only.';
const ERROR_USERNAME_TOO_LONG_30 =
  'Username length should not exceed 30 characters.';
const ERROR_USERNAME =
  'Please enter valid username. It should contain only latin ' +
  'alphabet letters or numbers/dot and begin with the letter.';
const ERROR_USERNAME_ENDS_DOT = 'Username cannot end with the dot.';
const ERROR_USERNAME_MULTIPLE_DOTS =
  'Username cannot contain more than one dot character.';
const ERROR_FIRSTNAME_LENGTH_30 =
  'Firstname value length should not exceed 30 characters';
const ERROR_FIRSTNAME_FIRST_LETTER =
  'Firstname should begin with the letter.';
const ERROR_FIRSTNAME_NOT_VALID = 'Firstname is not valid.';
const ERROR_LASTNAME_LENGTH_30 =
  'Lastname value length should not exceed 30 characters';
const ERROR_LASTNAME_FIRST_LETTER =
  'Lastname should begin with the letter.';
const ERROR_LASTNAME_NOT_VALID = 'Lastname is not valid.';

module.exports = function getUserValidationError(user) {
  // Validate User data
  // return error text on first error or null
  const username = trim(user.username || '');
  const password = trim(user.password || '');
  const email = trim(user.email || '');
  const firstname = trim(user.firstname || '');
  const lastname = trim(user.lastname || '');

  if (
    username === '' ||
    password === '' ||
    email === ''
  ) {
    return ERROR_ALL_FIELDS_REQUIRED;
  }
  if (password.length < 8) {
    return ERROR_PASSWORD_TOO_SHORT_8;
  }
  if (!isEmail(email)) {
    return ERROR_EMAIL_NOT_VALID;
  }
  const { registerEmailsAllowed } = config;
  if (registerEmailsAllowed && registerEmailsAllowed.length > 0) {
    const emailsFound = registerEmailsAllowed.find(domain => {
      return email.endsWith(domain);
    });
    if (!emailsFound) {
      return ERROR_LIMITED_DOMAINS_ONLY;
    }
  }
  if (username.length > 30) {
    return ERROR_USERNAME_TOO_LONG_30;
  }
  if (!regexps.REGEXP_USERNAME.test(username)) {
    return ERROR_USERNAME;
  }
  if (username.endsWith('.')) {
    return ERROR_USERNAME_ENDS_DOT;
  }
  if (username.indexOf('.') !== -1) {
    if (username.indexOf('.') !== username.lastIndexOf('.')) {
      return ERROR_USERNAME_MULTIPLE_DOTS;
    }
  }
  if (firstname) {
    if (firstname.length > 30) {
      return ERROR_FIRSTNAME_LENGTH_30;
    }
    if (regexps.REGEXP_UNICODE_STRING.exec(firstname[0]) === null) {
      return ERROR_FIRSTNAME_FIRST_LETTER;
    }
    if (regexps.REGEXP_UNICODE_NAME.exec(firstname) === null) {
      return ERROR_FIRSTNAME_NOT_VALID;
    }
  }
  if (lastname) {
    if (lastname.length > 30) {
      return ERROR_LASTNAME_LENGTH_30;
    }
    if (regexps.REGEXP_UNICODE_STRING.exec(lastname[0]) === null) {
      return ERROR_LASTNAME_FIRST_LETTER;
    }
    if (regexps.REGEXP_UNICODE_NAME.exec(lastname) === null) {
      return ERROR_LASTNAME_NOT_VALID;
    }
  }

  return null;
}

