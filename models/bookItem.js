const mongoose = require('mongoose');
const constants = require('../lib/constants');

const BookItemSchema = mongoose.Schema({
  book_id: {type: mongoose.Schema.Types.ObjectId, index: true, ref: 'Book'},
  filepath: String,
  base_content: String,
  current_content: String,
  eol_type: {
    type: String,
    enum: [constants.CRLF, constants.LF],
    default: 'LF',
  },
  date_updated: Date,
},
{
  collection: 'book_items',
  autoIndex: true,
});

module.exports = mongoose.model('BookItem', BookItemSchema); 
