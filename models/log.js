const mongoose = require('mongoose');

const LogSchema = mongoose.Schema({
  message: String,
  timestamp: {type: Date},
  level: String,
  meta: mongoose.Schema.Types.Mixed,
  url: String,
},
{
  collection: 'logs',
  autoIndex: false
});

module.exports = mongoose.model('Log', LogSchema);
 
