const mongoose = require('mongoose'),
      bcrypt = require('bcrypt');

const UserSchema = mongoose.Schema({
  username: {type: String, index: {unique: true}},
  email: {type: String, index: {unique: true}},
  password: String,
  firstname: String,
  lastname: String,
  description: String,
  role: {type: String, enum: ['admin', 'operator'], default: 'operator'},
  created: {type: Date, default: Date.now},
  // Activation
  activated: {type: Date, default: null},
  active: {type: Boolean, default: false},
  activation_string: {type: String, default: null},
  activation_string_issued: {type: Date, default: null},
  // Password reset
  password_reset_string: String,
  password_reset_string_issued: Date,
  password_reset_done: Date,
},
{
  collection: 'users',
  autoIndex: true
});

UserSchema.methods.validPassword = function(password, next) {
  return bcrypt.compare(password, this.password, function(err, res) {
      return next(res);
  });
}

module.exports = mongoose.model('User', UserSchema);
 
