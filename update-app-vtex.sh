#!/bin/bash

set -e

WORK_DIR=`pwd`

# Update source code
git pull
cd client && git pull

# Docker
cd $WORK_DIR
docker-compose -f docker-compose-vtex.yml build
docker-compose -f docker-compose-vtex.yml up -d app
