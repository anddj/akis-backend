AKIS backend application
========================

This application created to assist users in [creating book index](https://en.wikipedia.org/wiki/Index_(publishing)#Indexing_process) process.

Part of AKIS book indexing platform:

```
  +---------------+     +------------------+     +-----------------------+
  |               |     |                  |     |                       |
  | Frontend app. +-----+   Backend app.   +-----+ Book indexing workers |
  |               |     |                  |     |                       |
  +---------------+     +------------------+     +-----------------------+
                                  ^
                                  |
                                  --- We are here!
```

Backend technology stack:

  - [Node](https://nodejs.org)
  - [Express](https://expressjs.com)
  - [JWT](https://jwt.io/introduction)
  - [MongoDB](https://www.mongodb.com)
  - [Mongoose](https://mongoosejs.com)

Development
-----------

Clone the repository to your local environment.

Copy and edit configuration file:
```
cp conf/config.EXAMPLE.js conf/config.js
```

Run install ([yarn](https://yarnpkg.com/en/docs/install) is recommended):
```
yarn install
```

Starting development environment:
```
yarn server
```

directory structure:
```bash
_admin_scripts/             # some dockerized db maintenance scripts
conf/                       # configuration files
docker-compose-cloud.yml    # 
docker-compose-dev.yml      # docker-compose-*.yml (depending on deployment mode)
docker-compose-vtex.yml     # 
Dockerfile                  # application docker file
examples/                   # examples data files
initial-user-files/         # initial data files
lib/                        # helper functions, libraries
models/                     # mongoose models
package.json                # node application package.json file
package-lock.json           # node application package.json lock file
public/                     # public files
README.md                   # README file
routes/                     # node express routes (REST API end points implementation)
server.js                   # main server script
```

Production build
----------------

Dockerfile assuming frontend app is placed in `./client` dir.
```
docker-compose -d docker-compose-[MODE].yml build
docker-compose -d docker-compose-[MODE].yml up -d
```
