const logger = require('winston');
const createTransport = require('nodemailer').createTransport;

const config = require('../conf/config');

const transporter = createTransport(
  config.smtp, config.smtpDefaults);

module.exports = (data, cb) => {
  if (!data) throw Error('No mail data found');
  if (!data.subject) throw Error('No mail subject data found');
  if (!data.text) throw Error('No mail body text found');
  if (!data.to) throw Error('No mail TO: found');

  const mailData = {};

  if (data.from) {
    mailData.from = data.from;
  }

  transporter.sendMail(data, (err, info, response) => {
    if (err) {
      logger.error(err);
      if (cb) cb(err, null);
      return;
    }
    if (cb) cb(null, true);
  });

} 


