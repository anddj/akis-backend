const fs = require('fs');

const constants = require('./constants');
const logger = require('winston');

exports.sendError = (
  err,
  req,
  res,
  maskError=false,
  meta
) => {
  if (meta) {
    logger.error({
      meta,
      message: String(err),
    });
  } else {
    logger.error(String(err));
  };
  if (maskError) {
    return res.status(500).json({error: 'Server error.'});
  } else {
    return res.status(400).json({error: String(err)});
  }
}

exports.copyFile = (source, target, cb) => {
  let cbCalled = false;

  const rd = fs.createReadStream(source);
  rd.on('error', function(err) {
    done(err);
  });
  const wr = fs.createWriteStream(target);
  wr.on('error', function(err) {
    done(err);
  });
  wr.on('close', function(ex) {
    done();
  });
  rd.pipe(wr);

  function done(err) {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}

exports.detectNewLine = (str) => {
  // Detect first end line type
  const matched = str.match(/\r\n|\n/);
  const result = {
    '\n': constants.LF,
    '\r\n': constants.CRLF,
  }[matched]
  if (matched) {
    return result;
  }
  return null;
}
