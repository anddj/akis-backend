const User = require('../models/user'),
      jwt = require('jwt-simple'),
      config = require('../conf/config'),
      logger = require('winston');

module.exports = function(req, res, next) {
  const token = (req.body && req.body.access_token) ||
    (req.query && req.query.access_token) ||
    req.headers['x-access-token'];
  let decoded;
  if (token) {
    try {
      decoded = jwt.decode(token, config.web.jwtTokenSecret);
    } catch (err) {
      logger.error(err.message);
      return next(err.message);
    }
    if (!decoded) return next('Token error');
    if (decoded.exp <= Date.now()) {
      return res.status(400).end('Access token has expired');
    }
    User.findOne({_id: decoded.iss, active: true}, function(err, user) {
      if (!user) {
          return res.status(400).end('User not found for the access token ('+
                                     decoded.iss+')');
      };
      logger.info({message: 'JWT auth ok', meta: {username: user.username}});
      req.user = user;
      next();
    });
  } else {
    return res.status(400).end('Access token not provided');
  }
}
