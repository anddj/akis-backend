const MongoDB = require('winston-mongodb'),
      winston = require('winston');
      
const config = {
  db: {},
  web: {},
  mongoose: {},
  logger: {},
  smtp: {},
  smtpDefaults: {},
  graylog: {}
};

config.vtexMode = false;

config.siteTitleShort = 'AKIS';
config.siteTitleLong = 'subject indexing service';

// Web site base url (https://...)
config.baseUrl = '';

// Emails domains allowed to register
// e.g. ['example.com', 'example2.com']
config.registerEmailsAllowed = [];

// Rate limits
config.newUserLimitRateEnabled = false;
config.newUserLimitRate = 10; // successfull user registrations per interval 
config.newUserLimitInterval = 60 * 60 * 1; // interval, secs

config.verifyEmailTimeAllowed = 60 * 60 * 3; // secs
config.resetPasswordTimeAllowed = 60 * 60 * 3; // secs

config.textmining = {};
config.textmining.api = 'http://textmining.lt:8010';

// Nodejs server timeout
config.serverTimeout = 20 * 60 * 1000;

// SMTP, nodemailer options
// Docker note:
//  - bind sendmail to listen to 172.17.0.1:25
//  - configure here { host: '172.17.0.1', port: 25 }
config.smtp = {
  host: 'smtp.vtex.vmt',
  port: 25,
  secure: false,
  debug: true,
  tls: {rejectUnauthorized: false},
  // Uncomment and edit only if use 'login' type auth
  //auth: {
  //  user: '',
  //  pass: '',
  //}
};
// SMTP, nodemailer defaults
config.smtpDefaults = {
  from: '',
};

// Graylog
config.graylog = {
  host: 'alog.vtex.vmt',
  port: 1517
};

// DB
// production - used in docker environment
config.db.host = process.env.NODE_ENV === 'production'
  ? 'db'
  : '127.0.0.1';

config.db.name = 'akis';
config.db.port = '27017';
config.db.uri = 'mongodb://' +
  config.db.host +
  ':' + config.db.port +
  '/' + config.db.name;

// Mongoose
config.mongoose.debug = true;

// Web server application
config.web = {
  port: process.env.PORT || 3001,
  itemsPerPage: 100,
  jwtTokenSecret: 'REPLACE_WITH_SOME_STRONG_SECRET!', // use string length > 32
  jwtTokenExpiry: 30, // days
  jwtTokenExpiryClient: 30*12*50, // days (for command line clients)
  host: 'http://akis.vtex.vmt' // host name for command line clients
};

// Logger
const loggerDBOptions = {
  level: 'info',
  format: winston.format.simple(),
  capped: true,
  silent: false,
  db: config.db.uri,
  collection: 'logs',
  safe: false,
  handleExceptions: true,
  timestamp: true,
  options: {useNewUrlParser: true},
  label: false,
};

config.winstonConfig = {
  levels: winston.config.syslog.levels,
  format: winston.format.json(),
  transports: [
    new (winston.transports['MongoDB'])(loggerDBOptions),
    new (winston.transports.Console)({
      level: 'debug',
      format: winston.format.simple(),
      handleExceptions: true,
      timestamp: true,
    }),
  ],
}


module.exports = config;
