const formidable = require('formidable');
const fs = require('fs');
const isText = require('istextorbinary').isText;
const logger = require('winston');
const mongoose = require('mongoose');
const path = require('path');
const qs = require('querystring');
const yauzl = require('yauzl');
const yazl = require('yazl');
const { Writable } = require('stream');

const Book = require('../models/book');
const BookItem = require('../models/bookItem');
const config = require('../conf/config');
const constants = require('../lib/constants');
const { sendError, detectNewLine } = require('../lib/utils');


const TIMEOUT = 20 * 60 * 1000;
const win2linEOL = (str) => {
  return str.replace(/\r\n|\r/g, '\n');
};
const lin2winEOL = (str) => {
  return str.replace(/\n/g, '\r\n');
};

class BookItemWritable extends Writable{

  constructor(options) {
    super(options);
    this.fileContent = '';
  }

  _write(chunk, encoding, callback) {
    this.fileContent += chunk.toString();
    callback();
  }
};

const _createBookItems = (req, res, fields, files, bookId) => {
  const bookItems = [];
  const meta = {};

  yauzl.open(
    files.zipFile.path,
    { lazyEntries: true },
    (err, zipFile) => {
      if (err) {
        console.trace();
        return sendError(err, req, res);
      };
      zipFile.on('error', (err) => {
        return sendError(err, req, res);
      });
      zipFile.readEntry();
      zipFile.on('entry', (entry) => {
        if (/\/$/.test(entry.fileName)) {
          // Directory file names end with '/'.
          // Note that entires for directories themselves are optional.
          // An entry's fileName implicitly requires its parent
          // directories to exist.
          zipFile.readEntry();
        } else {
          // file entry
          zipFile.openReadStream(entry, (err, readStream) => {
            if (err) {
              return sendError(err, req, res);
            };
            readStream.on('end', () => {
              readStream.destroy();
              const content = bookItemWritable.fileContent;
              const { CRLF, LF } = constants;
              let lineEnding = LF;
              let isTextRes = null;
              let filenameExtOK = true;

              // Check if file is tex by extension (if required by request)
              if (fields.processTexOnly === 'true') {
                try {
                  const ext = path.extname(entry.fileName);
                  filenameExtOK = !!(ext && ext.toLowerCase() === '.tex');
                } catch(e) {
                  logger.error(e)
                }
              }
              if (filenameExtOK) {
                try {
                  lineEnding = detectNewLine(content) || LF;
                  const buf = Buffer.from(content);
                  isTextRes = isText(null, buf);
                } catch(e) {
                  logger.error(e);
                  console.trace();
                }
              }

              meta[entry.fileName] = {
                lineEnding,
                filenameExtOK,
                isText: isTextRes,
              };

              // Not a text content - reading next file
              if (!(isTextRes && filenameExtOK)) return zipFile.readEntry();

              const normalizedContent =
                lineEnding === CRLF ? win2linEOL(content) : content;

              bookItem = new BookItem({
                book_id: bookId,
                filepath: entry.fileName,
                base_content: normalizedContent,
                current_content: normalizedContent,
                eol_type: lineEnding,
                date_updated: new Date(),
              });
              bookItem.save( (err, data) => {
                if (err) {
                  return sendError(err, req, res, true);
                };
                bookItems.push(data);
                zipFile.readEntry();
              });
            });
            readStream.on('error', (err) => {
              return sendError(err, req, res);
            });
            bookItemWritable = new BookItemWritable();
            readStream.pipe(bookItemWritable);
          });
        };
      });
      zipFile.on('end', (err, zipFile) => {
        if (err) {
          return sendError(err, req, res);
        };
        fs.unlink(files.zipFile.path, (err) => {
          if (err) {
            return sendError(err, req, res);
          };
          return res.status(201).json({
            bookItems,
            meta,
            message: 'Created.',
          });
        });
      });

  });

}

const _createBook = (req, res) => {
  const form = new formidable.IncomingForm();
  form.parse(req, (err, fields, files) => {
    if (err) {
      return sendError(err, req, res);
    };
    const newBook = new Book({
      user_id: req.user._id,
      publisher: fields.publisher || '',
      project: fields.project || '',
      date_updated: new Date(),
      is_finished: false,
    });
    newBook.save( (err, data) => {
      if (err) {
        return sendError(err, req, res, true);
      };
      const bookId = data._id;
      _createBookItems(req, res, fields, files, bookId);
    })

  });
}

exports.create = (req, res) => {
  // Clear user's current book
  Book.findOneAndRemove({user_id: req.user._id}, (err, data) => {
    if (err) {
      return sendError(err, req, res);
    };
    if (data) {
      // Clear related book items
      BookItem.remove({book_id: data._id}, (err) => {
        if (err) {
          return sendError(err, req, res);
        };
        _createBook(req, res);
      });
    } else {
      _createBook(req, res);
    };
  });
};

exports.update = (req, res) => {
  Book.findOneAndUpdate(
    {user_id: req.user.id},
    req.body,
    {new: true},
    function(err, doc) {
      if (err) {
        logger.error('Error updating book', err.message);
        return res.status(500).end();
      };
      if (!doc) {
        return res.status(404).end();
      };
      return res.json(doc);
    }
  );
};

exports.getZip = (req, res) => {
  req.setTimeout(TIMEOUT);
  const userId = req.user._id;
  const dbQuery = {
    user_id: userId,
    is_finished: true,
  };
  if (req.query) {
    const { checkfinished } = req.query;
    if (checkfinished === 'false') {
      delete dbQuery.is_finished;
    };
  };
  Book.findOne(dbQuery, (err, data) => {
    if (err) {
      return sendError(err, req, res);
    };
    if (!data) {
      return res.status(404).json({message: 'Not found'});
    };
    const bookItemDbQuery = config.vtexMode
      ? {
          $and: [
            {book_id: data._id},
            // Only changed items
            {$expr: {$ne: ['$base_content', '$current_content']}}
          ]
        }
      : { book_id: data._id }; // all items
    BookItem.find(bookItemDbQuery, (err, data) => {
      if (err) {
        return sendError(err, req, res);
      };
      if (!data || data.length === 0) {
        return res.status(404).json({message: 'Not found'});
      };

      // Create zip file
      const zipfile = new yazl.ZipFile();
      res.setHeader('Content-Type', 'application/zip');
      zipfile.outputStream.pipe(res).on('finish', () => {
        logger.info('Done returning ZIP file to client');
      });

      data.forEach( ( bookItem ) => {
        // If vtexMode: return only changed files
        const contentChanged =
          bookItem.base_content !== bookItem.current_content
        if (contentChanged || !config.vtexMode) {
          const content = bookItem.eol_type === constants.CRLF
            ? lin2winEOL(bookItem.current_content)
            : bookItem.current_content;
          zipfile.addBuffer(
            Buffer.from(content),
            bookItem.filepath,
            {
              mtime: new Date(),
              mode: parseInt('0100664', 8),
            }
          );
        }
      });
      // Call end() after all files have been added.
      zipfile.end();
    });
  });
};

exports.search = (req, res) => {
  const userId = req.user._id;
  Book.findOne({user_id: userId}, (err, data) => {
    if (err) {
      return sendError(err, req, res);
    };
    if (!data) {
      return res.status(404).json({message: 'Not found'});
    };
    return res.json(data);
  });
};
