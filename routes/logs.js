const mongoose = require('mongoose');
const sanitize = require('sanitize-filename');
const logger = require('winston');

const Log = require('../models/log');
const itemsPerPage = 30;

const getRegexp = function(str) {
    const rgx = '^' + str + '.*';
    return {'$regex': rgx, '$options': 'i'};
};

exports.search = function(req, res) {
  const q = req.query || {};
  const qs = {};
  const qDB = {};
  const pageNr = Number(q.pageNr) || 1;
  let dateFrom = new Date();
  if (q.dateFrom) {
    try {
      dateFrom = new Date(q.dateFrom);
    } catch(e) {
      logger.error(String(e));
    }
  };
  qDB.timestamp = {'$lt': dateFrom};
  if (q.onlyErrors && q.onlyErrors === 'true') {
    qDB.level = 'error';
  };
  qs['timestamp'] = -1;
  Log.countDocuments(qDB, function(err, count) {
    if (err) {
      logger.error('Error getting logs count ', err.message);
      return res.status(500).end();
    }
    Log.find(qDB)
      .sort(qs)
      .skip((pageNr-1)*itemsPerPage)
      .limit(itemsPerPage)
      .exec(function(err, logs) {
      if (err) {
        logger.error('Error executing DB query for ', qDB, err.message);
        return res.status(500).end();
      }
      const doc = {
        'items': logs,
        'meta': {
          'totalItems': count,
          'itemsPerPage': itemsPerPage,
          'pageNr': pageNr,
          'dateFrom': dateFrom,
        },
      };
      res.json(doc);
      });
  });
};

exports.getZip = function(req, res) {
  const { logId } = req.params;
  if (!logId) return res.status(400).end();
  Log.findById(logId, function(err, data) {
    if (err) {
      logger.error('Error getting logs count ', err.message);
      return res.status(500).end();
    };
    if (!(data.meta && data.meta.zipFile)) {
      return res.status(404).end();
    };
    const zipFilePath = __dirname +
      '/../failed/' +
      sanitize(data.meta.zipFile);
    res.download(zipFilePath);
  });
};
