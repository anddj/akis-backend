const compare = require('bcrypt').compare;
const hash = require('bcrypt').hash;
const hashSync = require('bcrypt').hashSync;
const isAlphanumeric = require('validator').isAlphanumeric;
const isEmail = require('validator').isEmail;
const isEmpty = require('validator').isEmpty;
const isLength = require('validator').isLength;
const jwt = require('jwt-simple');
const logger = require('winston');
const moment = require('moment');
const mongoose = require('mongoose');
const promisify = require('util').promisify;
const fs = require('fs');

const config = require('../conf/config');
const User = require('../models/user');
const UserSettings = require('../models/userSettings');
const userValidate = require('../models/userValidate');
const Book = require('../models/book');
const BookItem = require('../models/bookItem');
const sendMail = require('../lib/sendMail');

const hashPromise = promisify(hash);
const comparePromise = promisify(compare);

let newUserCounter = 0;
let newUserLastDate = moment();

const validateUser = function() {
  return false;
}

function makeRandomString() {
  const possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let randomText = '';
  for (let i = 0; i < 40; i++) {
    randomText +=
      possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return randomText; 
}

async function removeExpiredUsers() {
  // Find expired, not registered users
  const expiryDate = new Date() - config.verifyEmailTimeAllowed*1000;
  await User.remove({
    active: false,
    activated: null,
    activation_string: {'$ne': null},
    activation_string_issued: {
        '$ne': null,
        '$lt': expiryDate,
    },
  }, (err) => {
    if (err) {
      logger.error(err);
    }
  });
}

exports.registerNewUser = async (req, res) => {
  logger.info('Received register new user request');

  if (config.newUserLimitRateEnabled) {
    // Check the limits, return "Too many requests" status if overflowed
    const now = moment();
    if ((now - newUserLastDate) >
          config.newUserLimitInterval * 1000) {
      newUserLastDate = moment();
      newUserCounter = 0;
    } else {
      if (newUserCounter > config.newUserLimitRate) {
        logger.warning('New user register limit reached. ' +
          'Current limit: ' +
          config.newUserLimitRate + ' successfull user registrations per ' +
          config.newUserLimitInterval + ' secs.');
         return res.status(429).end();
      }
    }
  }

  // Self cleanup not registered users
  await removeExpiredUsers().catch(err=>logger.error(e));

  // Validate
  const userData = req.body;
  if (!userData) return res.status(400).end();
  const error = userValidate(userData);
  if (error) {
    logger.error(String(error))
    return res.status(400).end();
  }
  User.findOne({'$or': [
    {username: userData.username},
    {email: userData.email},
  ]},
  (err, user) => {
    if (err) {
      logger.error(err);
      return res.status(500).end();
    }
    if (user) {
      if (user.username && (user.username === userData.username)) {
        res.set('x-akis-conflict', 'Username already exists.');
        return res.status(409).end();
      }
      if (user.email && (user.email === userData.email)) {
        res.set('x-akis-conflict', 'Email address already exists.');
        return res.status(409).end();
      }
    }

    try {
      userData.password = hashSync(userData.password, 10);
      // User activation string
      userData.activation_string = makeRandomString();
      userData.activation_string_issued = new Date();
      // Some data escaped
      const escapedFields = ['firstname', 'lastname', 'description'];
      escapedFields.forEach( val => {
        if (userData[val]) {
          userData[val] = escape(userData[val]);
        }
      });
    } catch(err) {
      logger.error(err);
      return res.status(500).end();
    }
    const newUser = new User(userData);
    newUser.save( (err, usr) => {
      if (err) {
        logger.error(err);
        return res.status(500).end();
      }
      if (config.newUserLimitRateEnabled) {
        newUserCounter += 1;
      }
      // Send confirm link by email
      try {
        if (!usr.activation_string) throw Error('No user activation string');
        const mailData = {};
        const { baseUrl, siteTitleLong, siteTitleShort } = config;
        mailData.from = config.smtpDefaults.from;
        mailData.to = usr.email;
        mailData.subject = siteTitleShort + ' sign up, please confirm';
        mailData.text = 'Welcome to ' + siteTitleShort +
          ' text indexing service!\n\n';
        mailData.text += 'To verify your email address ';
        mailData.text += 'please click the following link (or copy/paste' +
          ' it to your internet browser):\n' +
          baseUrl + '/verify/' + usr.activation_string + '\n\n';
        mailData.text +=
          '(the link is valid for limited amount of time).\n\n\n';
        mailData.text += '-- \n';
        mailData.text += siteTitleShort + ' (' + siteTitleLong + ').';
        sendMail(mailData, (err) => {
          if (err) {
            logger.error('Failed to send mail');
            // Remove the doc if no mail sent
            if (usr) {
              usr.remove(err => {
                if (err) {
                  logger.error(err);
                  return res.status(500).end();
                }
              });
            } else {
              res.status(500).end();
            }
          } else {
            // No error
            res.status(201).json({message: 'Created'});
          }
        });
      } catch(e) {
        logger.error('Error while sending mail');
        logger.error(e)
        // Remove the doc if no mail sent
        if (usr) {
          usr.remove(err => {
            if (err) {
              logger.error(err);
              return res.status(500).end();
            }
          });
        } else {
          res.status(500).end();
        }
      }
    });
  });
}

// Verify email, create default user content
exports.verifyEmail = async (req, res) => {
  console.info('Verify email request received');

  const { hash } = req.params;

  if (!hash || !isAlphanumeric(hash)) {
    return res.status(400).end();
  }

  let user;
  await User.findOne({active: false, activation_string: hash}).then(uData => {
    user = uData;
  }).catch(err => {
    logger.error(err);
    throw err;
  });
  if (!user) {
    logger.error('User not found for the verification string.');
    return res.status(400).end();
  }
  try {
    const { activation_string_issued } = user;
    if (!activation_string_issued) {
      logger.error('User activation string not found.')
      return res.status(500).end();
    }
    if (new Date() - activation_string_issued
      > config.verifyEmailTimeAllowed * 1000) {
      logger.error('Verification link has been expired.');
      return res.status(410).json({message: 'Link has been expired.'});
    }
    user.active = true;
    user.activated = new Date();
    user.activation_string = null;
    let usr;
    await user.save().then( userData => {
      usr = userData;
    }).catch(err => {
      logger.error(err);
      throw err;
    });

    // Save default user settings
    try {
      const newUserSettings = new UserSettings({user_id: usr.id});
      await newUserSettings.save();
      // Add default book/bookitems
      let newBookId;
      await Book.create({
        user_id: usr.id,
        zip_name: 'subject-index.zip',
        date_updated: new Date(),
      }).then( bk => {
        newBookId = bk._id;
      });
      if (!newBookId) {
        throw new Error('Error while creating default book');
      }
      const readdirPromise = promisify(fs.readdir);
      const readFilePromise = promisify(fs.readFile);
      let filenames;
      await readdirPromise('initial-user-files')
        .then( (names) => {
          filenames = names;
        });
      if (!filenames) throw new Error('Error reading doc files');
      for (const filename of filenames) {
        await readFilePromise('initial-user-files/'+filename)
          .then(async content => {
            await BookItem.create({
              book_id: newBookId,
              filepath: filename,
              base_content: content,
              current_content: content,
              eol_type: 'LF',
              date_updated: new Date(),
            }).catch(err => {
              logger.error(err);
              throw err;
            })
          }).catch( err => {
            logger.error(err);
            throw err;
          });
        }
      } catch(err) {
        logger.error(String(err));
        throw err;
      }
      return res.json({message: 'User verification succeded.'});
  } catch(e) {
    logger.error(e);
    return res.status(500).end();
  }
}

exports.resetPassword = (req, res) => {
  logger.info('Received reset password request');
  try {
    const email = req.body.email;
    if (!email) {
      return res.status(400).json({error: 'No email address received'});
    }
    if (!isEmail(email)) {
      logger.error('Failed to verify email: ' + String(email));
      return res.status(400).json({error: 'No valid email address received'});
    }
    User.findOne({email, active: true}, (err, user) => {
      if (err) {
        return res.status(500).end();
      }
      if (!user) {
        return res.status(404).json({error: 'No user found.'});
      }

      try {
        // Prevent repeated password resets
        const { password_reset_string, password_reset_string_issued } = user;
        if (password_reset_string &&
            password_reset_string_issued &&
            (new Date() - password_reset_string_issued <
              config.resetPasswordTimeAllowed*1000)
        ) {
          return res.status(429).json({
            error: 'You are not allowed to reset password now. Try later.'
          });
        }
      } catch(err) {
        if (err) logger.error(String(err));
        return res.status(500).end();
      }
      user.password_reset_string = makeRandomString();
      user.password_reset_string_issued = new Date();
      user.save( (err, usr) => {
        if (err) {
          logger.error(err);
          return res.status(500).end();
        }

        // Send email
        if (!usr.password_reset_string) {
          throw Error('No password reset string');
        }
        const mailData = {};
        const { baseUrl, siteTitleShort, siteTitleLong } = config;
        mailData.from = config.smtpDefaults.from;
        mailData.to = usr.email;
        mailData.subject = siteTitleShort + ' password reset request';
        mailData.text = 'We received the password reset request.\n\n';
        mailData.text += 'To reset your password, please click on the ';
        mailData.text += 'link below (or copy/paste it to your internet ';
        mailData.text += 'browser) :\n';
        mailData.text += baseUrl + '/reset/'
          + usr.password_reset_string + '\n\n';
        mailData.text +=
          '(the link is valid for limited amount of time).\n\n\n';
        mailData.text += '-- \n';
        mailData.text += siteTitleShort + ' (' + siteTitleLong + ').';
        sendMail(mailData, (err) => {
          if (err) {
            logger.error('Failed to send mail');
            return res.status(500).end();
          }
          res.json({message: 'Email sent'});
        });

      });
      
    });

  } catch(e) {
    logger.error(e);
    res.status(500).json({error: 'Error while processing request'});
  }
}

exports.resetPasswordDB = (req, res) => {
  const { password } = req.body;
  const { hash } = req.params;

  if (!(hash
    && typeof(hash) === 'string'
    && isAlphanumeric(hash)
    && password
    && typeof(password) === 'string'
  )) {
    return res.status(400).end();
  }
  
  const emptyOptions = { ignore_whitespace: true };
  const lengthOptions = { min: 8 };
  if (isEmpty(password, emptyOptions) || !isLength(password, lengthOptions)) {
    logger.error('Reset password: failed password length constraints.');
    return res.status(400).end();
  }

  const { resetPasswordTimeAllowed } = config;
  try {
    User.findOne({active: true, password_reset_string: hash}, (err, user) => {
      if (err) {
        logger.error(err);
        return res.status(500).end();
      }
      if (!user) {
        logger.error('User not found for the verification string.');
        return res.status(400).end();
      }
      try {
        const { password_reset_string_issued } = user;
        if (!password_reset_string_issued) {
          logger.error('Password reset string not found.')
          return res.status(500).end();
        }
        if (new Date() - password_reset_string_issued
          > resetPasswordTimeAllowed * 1000) {
          logger.error('Reset password link has been expired.');
          return res.status(410).json({message: 'Link has been expired.'});
        }
        user.password = hashSync(password, 10);
        user.password_reset_done = new Date();
        user.password_reset_string = null;
        user.save( (err, usr) => {
          if (err) {
            logger.error(err);
            return res.status(500).end();
          }
          return res.json({message: 'Password reset succeded.'});
        });
      } catch(e) {
        return res.status(500).end();
      }
    });
  } catch(e) {
    logger.error(e);
    res.status(500).json({error: 'Error while processing request'});
  }
};

// Change user password upon request
exports.changePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;

  if (!(
    oldPassword && typeof(oldPassword) === 'string' &&
    newPassword && typeof(newPassword) === 'string' &&
    newPassword !== oldPassword
  )) {
    return res.status(400).end();
  }

  const emptyOptions = { ignore_whitespace: true };
  const lengthOptions = { min: 8 };
  if (
    isEmpty(oldPassword, emptyOptions) ||
    !isLength(oldPassword, lengthOptions) ||
    isEmpty(newPassword, emptyOptions) ||
    !isLength(newPassword, lengthOptions)
  ) {
    logger.error('Reset password: failed password length constraints.');
    return res.status(400).end();
  }

  let user;
  await User.findOne({_id: req.user._id})
    .then( userData => {
      user = userData;
    })
    .catch( err => {
      logger.error(err);
    });

  if (!user) {
    return res.status(500).end();
  }

  let compareResult;
  await comparePromise(oldPassword, user.password)
    .then( result => {
      compareResult = result;
    })
    .catch( err => {
      logger.error(err)
    });

  if (compareResult !== true) {
    return res.status(400).end('Old password does not match');
  }

  let passwordHash;
  await hashPromise(newPassword, 10)
    .then( hashData => {
      passwordHash = hashData;
    })
    .catch( err => {
      logger.error(err)
    });

  if (!passwordHash) {
    return res.status(500).end();
  }

  user.password = passwordHash;

  let savedUser;
  await user.save()
    .then( (user) => {
      if (user) {
        savedUser = user;
      }
    })
    .catch( err => {
      logger.error(err)
    });

  if (!savedUser) {
    return res.status(500).end();
  }

  return res.json({message: 'OK'});
};
