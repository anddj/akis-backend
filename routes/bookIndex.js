const mongoose = require('mongoose');
const yazl = require('yazl');
const yauzl = require('yauzl');
const request = require('request');
const fs = require('fs');
const tmp = require('tmp');
const { Writable } = require('stream');
const path = require('path');
const logger = require('winston');

const config = require('../conf/config');
const Book = require('../models/book');
const BookItem = require('../models/bookItem');
const UserSettings = require('../models/userSettings');
const { sendError, copyFile } = require('../lib/utils');

const TIMEOUT = 20 * 60 * 1000;

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

class BookItemWritable extends Writable{

  constructor(options) {
    super(options);
    this.fileContent = '';
  }

  _write(chunk, encoding, callback) {
    this.fileContent += chunk.toString();
    callback();
  }
};

/*
 * POST
 *
 */

exports.buildIndex = (req, res) => {

  req.setTimeout(TIMEOUT);
  const startTime = new Date();
  let bookItemIdsToIndex = [];
  const bookItemPathsToIndex = {};
  const bookItemsResp = [];
  if (req.query.bookItemIdsToIndex &&
      req.query.bookItemIdsToIndex.length > 0) {
    bookItemIdsToIndex = req.query.bookItemIdsToIndex.concat();
  } else {
    return sendError('Book items IDs to index not found.', req, res);
  }
  const userId = req.user._id;

  UserSettings.find({user_id: userId}, (err, userSettings) => {
    if (err) {
      return sendError(err, req, res);
    };
    if (!userSettings || userSettings.length === 0) {
      return res.status(404).json({message: 'User settings not found'});
    };

    Book.findOne({user_id: userId}, (err, book) => {
      if (err) {
        return sendError(err, req, res);
      };
      if (!book) {
        return res.status(404).json({message: 'Not found'});
      };
      const bookId = book._id;
      const bookItemDbQuery = { book_id: bookId };


      BookItem.find(bookItemDbQuery, (err, bookItems) => {
        if (err) {
          return sendError(err, req, res);
        };
        if (!bookItems || bookItems.length === 0) {
          return res.status(404).json({message: 'Not found'});
        };

        // Create outgoing zip file
        const outgoingZipFile = new yazl.ZipFile();

        tmp.file( (error, outgoingZipFilePath) => {

          const writeStream = fs.createWriteStream(outgoingZipFilePath);

          outgoingZipFile.outputStream.pipe(writeStream).on('close', () => {

            reqs = request.post({
              uri: `${config.textmining.api}/buildIndex`,
              timeout: TIMEOUT,
              rejectUnauthorized: false,
            });

            reqs.on('error', function(err) {
              // Remove temp zip
              fs.unlink(outgoingZipFilePath, (error) => {
                if (error) {
                  logger.error(String(error));
                };
                return sendError(err, req, res);
              });
            });

            reqs.on('response', function(response) {
              const bufs = [];
              response.on('data', function(d) {
                bufs.push(d);
              });

              response.on('end', function() {

                const buf = Buffer.concat(bufs);

                const logMeta = {
                  username: req.user ? req.user.username : null,
                  confJson: userSettings[0].build_index_params || {},
                  filesToIndex:
                    Object.keys(bookItemPathsToIndex).join(','),
                  zipFile: path.basename(outgoingZipFilePath),
                  remoteService: {
                    path: reqs.path,
                    statusCode: response.statusCode,
                    message: response.message || '',
                  },
                };

                if (response.statusCode !== 200) {

                  copyFile(
                    outgoingZipFilePath,
                    'failed/'+path.basename(outgoingZipFilePath),
                    function(error) {
                      if (error) {
                        logger.error(String(error));
                      };

                      // Remove temp zip
                      fs.unlink(outgoingZipFilePath, (err) => {
                        if (err) {
                          return logger.error(String(err));
                        };
                      });

                      const errorBody  = response.headers['content-type']
                            .startsWith('text/plain') &&
                            buf.length < (1024 * 1024)
                          ? buf
                          : '';
                      return sendError(
                        errorBody,
                        req,
                        res,
                        false,
                        logMeta
                      );

                    }
                  );
                  
                  return;

                };

                // Remove temp zip
                fs.unlink(outgoingZipFilePath, (err) => {
                  if (err) {
                    return logger.error(String(err));
                  };
                });

                try {
                  yauzl.fromBuffer(buf,
                    {lazyEntries: true},
                    (err, zipIndexed) => {

                    if (err) {
                      return sendError(err, req, res);
                    };

                    zipIndexed.on('error', function(err) {
                      return sendError(err, req, res);
                    });
                      zipIndexed.on('end', function() {
                        const filesLength = bookItemsResp
                          .reduce(function(acc, item) {
                            return acc + item.current_content.length;
                          }, 0);
                        const ending = bookItemsResp.length > 1 ? 's' : '';
                        const duration = parseFloat(
                          (new Date() - startTime) / 1000).toFixed(2);
                        delete logMeta.zipFile; // we do not save zip's on OK
                        logger.info({ message: 'Build index duration: ' + duration +
                          ' secs (' +
                          String(bookItemsResp.length) + ' file' +
                          ending + ', annotated file' + ending + ' length: ' +
                          filesLength +' symbols)',
                          meta: logMeta,
                        });
                        return res.json({
                          message: 'OK',
                          annotatedFiles: bookItemsResp,
                        });
                    });
                    zipIndexed.readEntry();
                    zipIndexed.on('entry', function(entry) {

                      if (/\/$/.test(entry.fileName)) {
                        // Directory file names end with '/'.
                        // Note that entires for directories themselves
                        // are optional.
                        // An entry's fileName implicitly requires its parent
                        // directories to exist.
                        zipIndexed.readEntry();

                      } else {

                        if (typeof bookItemPathsToIndex[entry.fileName] !==
                          'undefined') {

                          zipIndexed.openReadStream(entry, (err, readStream) => {
                            if (err) {
                              return sendError(err, req, res);
                            };
                            readStream.on('end', () => {
                              readStream.destroy();

                              bookItem = new BookItem({
                                book_id: bookId,
                                filepath: entry.fileName,
                                current_content: bookItemWritable.fileContent,
                                date_updated: new Date(),
                              });

                              // Append to outgoing result data
                              bookItemsResp.push({
                                _id: bookItemPathsToIndex[entry.fileName],
                                book_id: bookId,
                                filepath: entry.fileName,
                                current_content: bookItemWritable.fileContent,
                              });

                              zipIndexed.readEntry();

                            });
                            readStream.on('error', (err) => {
                              return sendError(err, req, res);
                            })
                            bookItemWritable = new BookItemWritable();
                            readStream.pipe(bookItemWritable);
                          });
                        } else { // if in filesToIndex
                          zipIndexed.readEntry();
                        };
                      }; // else

                    });


                  });
                } catch(err) {
                  return sendError(err, req, res);
                }
              });
            });

            const form = reqs.form();
            form.append('zipFile', fs.createReadStream(outgoingZipFilePath));
            if (req.user && req.user.username) {
              form.append('user', req.user.username);
            };
            if (userSettings[0].build_index_params) {
              form.append('confJson',
                JSON.stringify(userSettings[0].build_index_params));
            };
            const filePaths = Object.keys(bookItemPathsToIndex).join(',');
            form.append('filesToIndex', filePaths);
          }); // zipFile output stream

          bookItems.forEach( ( bookItem ) => {
            // book item ID -> book item path resolution
            if (bookItemIdsToIndex.indexOf(String(bookItem._id)) !== -1) {
              bookItemPathsToIndex[bookItem.filepath] = String(bookItem._id);
            };
            outgoingZipFile.addBuffer(Buffer.from(bookItem.current_content),
                              bookItem.filepath, {
                                mtime: new Date(),
                                mode: parseInt('0100664', 8),
                              });
          });

          // Call end() after all files have been added.
          outgoingZipFile.end();

        });
      })
    })
  });


};



/*
 * GET
 *
 */
exports.getIndex = (req, res) => {
  req.setTimeout(TIMEOUT);
  const startTime = new Date();
  let bookItemIdsToIndex = null;
  if (req.query.bookItemIdsToIndex &&
      req.query.bookItemIdsToIndex.length > 0) {
    bookItemIdsToIndex = req.query.bookItemIdsToIndex;
  };
  const userId = req.user._id;
  Book.findOne({user_id: userId}, (err, data) => {
    if (err) {
      return sendError(err, req, res);
    };
    if (!data) {
      return res.status(404).json({message: 'Not found'});
    };
    const bookItemDbQuery = { book_id: data._id };
    if ( bookItemIdsToIndex ) {
      Object.assign(bookItemDbQuery, {_id: {'$in': bookItemIdsToIndex}});
    };
    BookItem.find(bookItemDbQuery, (err, data) => {
      if (err) {
        return sendError(err, req, res);
      };
      if (!data || data.length === 0) {
        return res.status(404).json({message: 'Not found'});
      };

      // Create zip file
      const zipfile = new yazl.ZipFile();

      tmp.file( (error, path) => {
        const writeStream = fs.createWriteStream(path);

        zipfile.outputStream.pipe(writeStream).on('close', () => {
          const reqs = request.post(
            { uri: config.textmining.api+'/getIndex.json',
              timeout: TIMEOUT,
            },
            (err, resp, body) => {
            if (err) {
              logger.error(String(err));
              fs.unlink(path, (err) => {
                return sendError(err, req, res);
              });
            } else {
              fs.unlink(path, (err) => {
                let bodyDoc = {};
                try {
                  bodyDoc = JSON.parse(body);
                } catch(e) {
                  logger.error(e);
                  return sendError(err, req, res);
                }
                const duration = parseFloat(
                  (new Date() - startTime) / 1000).toFixed(2);
                const logMeta = {
                  username: req.user ? req.user.username : null,
                  filesToIndex: filePathsList.join(','),
                  remoteService: {
                    path: reqs.path,
                    statusCode: resp.statusCode,
                    message: resp.message || '',
                  },
                };
                logger.info({message: 'Show index duration: ' + duration +
                  ' secs (index tree height: ' +
                  String(bodyDoc.Right ? bodyDoc.Right.length : '0') +
                  ')',
                  meta: logMeta,
                });
                return res.json({message: 'OK', indexData: bodyDoc});
              });
            }
          });
          const form = reqs.form();
          form.append('files', fs.createReadStream(path));
          if (req.user && req.user.username) {
            form.append('user', req.user.username);
          };
        });

        const filePathsList = [];
        data.forEach( ( bookItem ) => {
          filePathsList.push(bookItem.filepath);
          zipfile.addBuffer(Buffer.from(bookItem.current_content),
                            bookItem.filepath, {
                              mtime: new Date(),
                              mode: parseInt('0100664', 8),
                            });
        });

        // Call end() after all files have been added.
        zipfile.end();

      });
    })
  })
}
