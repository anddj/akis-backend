const mongoose = require('mongoose');
const logger = require('winston');

const Book = require('../models/book');
const BookItem = require('../models/bookItem');

exports.update = async (req, res) => {
  if (!req.body || req.body.length === 0) {
    return res.status(400).json({
      error: 'No book items data found in the request.'
    });
  };
  const updatedBookItems = [];
  const promises = req.body.map( async item => {
    const results = await BookItem.findOneAndUpdate(
      {_id: item._id, book_id: item.book_id},
      {current_content: item.current_content, date_updated: new Date()},
      {new: true}
    );
    updatedBookItems.push(results);
  });
  const results = await Promise.all(promises);
  if (updatedBookItems.length > 0) {
    return res.json({message: 'OK', bookItems: updatedBookItems});
  } else {
    return res.status(500).json({error: 'Server error'});
  }

}

exports.search = (req, res) => {
  const userId = req.user._id;
  Book.findOne({user_id: userId}, (err, data) => {
    if (err) {
      return res.status(500).json({error: 'Server error'});
    };
    if (!data) {
      return res.status(404).json({message: 'Not found'});
    };
    BookItem.find({book_id: data._id}, (err, data) => {
      if (err) {
        return res.status(500).json({error: 'Server error'});
      };
      if (!data) {
        return res.status(404).json({message: 'Not found'});
      };
      return res.json({message: 'OK', bookItems: data});
    })
  })
}
