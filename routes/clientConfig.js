const mongoose = require('mongoose'),
      moment = require('moment'),
      jwt = require('jwt-simple'),
      config = require('../conf/config'),
      logger = require('winston');

exports.search = (req, res) => {
  logger.info(
    'creating client config file',
    meta={
      username: req.user?req.user.username:null,
      query: req.query
    }
  );
  const expiry = parseInt(config.web.jwtTokenExpiryClient);
  const expires = moment().add(expiry, 'days').valueOf();
  const token = jwt.encode(
    {iss: req.user._id, exp: expires},
    config.web.jwtTokenSecret
  );
  const response =
`--api-key
${Buffer.from(token).toString('base64')}
--server-uri
${config.web.host}
--user
${req.user.username}
--graylog-host
${config.graylog.host}
--graylog-port
${config.graylog.port}
`

  return res.send(response)
}

