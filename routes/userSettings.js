const mongoose = require('mongoose');
const logger = require('winston');

const UserSettings = require('../models/userSettings');
const User = require('../models/user');
const Book = require('../models/book');
const BookItem = require('../models/bookItem');

exports.search = (req, res) => {
  const userId = req.user._id;
  UserSettings.findOne({user_id: userId}, (err, data) => {
    if (err) {
      logger.error(String(err));
      return res.status(500).end('Server error.');
    };
    if (!data) {
      return res.status(404).end('User settings not found for this user.');
    };
    return res.json(data);
  })
};

exports.update = (req, res) => {
  const userId = req.user._id;
  const {
    build_index_params,
    concordance_lines,
    dark_theme,
    font_scaling,
  } = req.body;
  UserSettings.findOneAndUpdate({
    user_id: userId,
  }, {
    build_index_params,
    concordance_lines,
    dark_theme,
    date_updated: new Date(),
    font_scaling,
    user_id: userId,
  }, {
    new: true, // the updated document will be returned from DB
    upsert: true,
  }, (err, data) => {
    if (err) {
      logger.error(String(err));
      return res.status(500).end('Server error.');
    };
    if (!data) {
      return res.status(404).end('User settings not found for this user.');
    };
    return res.json(data);
  });
};

exports.deleteUser = async (req, res) => {
  const userId = req.user._id;
  if (!userId) return res.status(500).end('User not found');
  logger.info(`Received user delete request (${userId})`);
  // Delete BookItems -> Book -> UserSettings -> User
  try {
    const books = await Book.find({user_id: userId});
    if (books && books.length === 1) {
      // Remove book items
      await BookItem.deleteMany({book_id: books[0]._id});
      await Book.deleteOne({_id: books[0]._id});
    }
    await UserSettings.deleteOne({user_id: userId});
    await User.deleteOne({_id: userId});
    return res.json({
      message: 'User deleted',
    })
  } catch(e) {
    logger.error(e.stack);
    return res.status(500).end(e.message);
  }
};
