// server.js

const basicAuth = require('express-basic-auth');
const bodyParser = require('body-parser');
const compression = require('compression');
const express = require('express');
const helmet = require('helmet');
const jwt = require('jwt-simple');
const moment = require('moment');
const mongoose = require('mongoose');
const path = require('path');
const winston = require('winston');
const promiseRetry = require('promise-retry')

const config = require('./conf/config');
const User = require('./models/user');
const UserSettings = require('./models/userSettings');
const books = require('./routes/books');
const bookItems = require('./routes/bookItems');
const userSettings = require('./routes/userSettings');
const userManagement = require('./routes/userManagement');
const clientConfig = require('./routes/clientConfig');
const bookIndex = require('./routes/bookIndex');
const logs = require('./routes/logs');
const jwauth = require('./lib/jwtauth');

// Logger
winston.configure(config.winstonConfig);
const logger = winston;


const app = express();
// Helmet middleware to construct secure headers
app.use(helmet());
const router = express.Router();

// Compression middleware
app.use(compression());

router.use( (req, res, next) => {
  logger.info([req.ip, req.method, req.url].join(' '));
  next();
});

app.use(express.static(path.join(__dirname, 'client/build')));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api', router);
app.set('jwtTokenSecret', config.web.jwtTokenSecret);

// Body parser middleware
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// API endpoints

function authorizer(username, password, cb) {
  User.findOne({
    active: true,
    '$or': [
      {username},
      {email: username},
    ]
    }, (err, user) => {
      if (err) throw err;
      if (!user) return cb(null, false);
      return user.validPassword(password, isValid => {
        return cb(null, isValid);
      });
  });
}

const asyncAuth = basicAuth({
  authorizer: authorizer,
  authorizeAsync: true
});

// JWT Token, basic auth required
app.get('/api/token', asyncAuth, (req, res, next) => {
  return User.findOne({
    active: true,
    '$or': [
      {username: req.auth.user},
      {email: req.auth.user},
    ]
    }, (err, user) => {
      if (err) return next(err);
      if (!user) return res.status(401).json({error: 'Wrong credentials.'});
      const expiry = parseInt(config.web.jwtTokenExpiry);
      const expires = moment().add(expiry, 'days').valueOf();
      const token = jwt.encode({iss: user._id, exp: expires},
                               app.get('jwtTokenSecret'));
      const userData = user.toJSON();
      return res.json({
          token: token,
          expires: expires,
          user: { username: userData.username },
      });
  });
});

// Logs
app.get('/api/logs', [jwauth], (req, res) => {
  if (req.user['role'] !== 'admin') {
    return res.status(403).send('Insufficient rights.');
  }
  return logs.search(req, res);
});
app.get('/api/logs/:logId/zip', [jwauth], (req, res) => {
  if (req.user['role'] !== 'admin') {
    return res.status(403).send('Insufficient rights.');
  }
  return logs.getZip(req, res)
});

// Books
//   .../books -> ZIP API
//   .../booksmeta -> books metadata
app.get('/api/books', [jwauth], books.getZip);
app.post('/api/books', [jwauth], books.create);
app.get('/api/booksmeta', [jwauth], books.search);
app.patch('/api/booksmeta', [jwauth], books.update);

// Book items
app.get('/api/bookitems', [jwauth], bookItems.search);
app.patch('/api/bookitems', [jwauth], bookItems.update);

// User settings
app.get('/api/usersettings', [jwauth], userSettings.search);
app.patch('/api/usersettings', [jwauth], userSettings.update);
app.delete('/api/usersettings', [jwauth], userSettings.deleteUser);

// User management (register new user, reset/change user password)
app.post('/api/userregister', userManagement.registerNewUser);
app.post('/api/userreset', userManagement.resetPassword);
app.post('/api/userreset/:hash', userManagement.resetPasswordDB);
app.post('/api/userchangepwd', [jwauth], userManagement.changePassword);
// Verify email
app.post('/api/verify/:hash', userManagement.verifyEmail);

// Book index
app.get('/api/bookindex', [jwauth], bookIndex.getIndex);
app.post('/api/bookindexbuild', [jwauth], bookIndex.buildIndex);

// Client config file
if (config.vtexMode === true) {
  app.get('/api/clientconfig', [jwauth], clientConfig.search);
}

app.all('/api/*', (req, res) => {
  res
    .status(404)
    .json({message: `API path not found: ${req.originalUrl}`})
});

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});

mongoose.connection.on('disconnected', () => {
  logger.debug('Mongoose default connection to DB disconnected');
});

mongoose.connection.on('connected', () => {
  const port = config.web.port;
  const server = app.listen(port, '0.0.0.0', err => {
    if (err) {
      throw err;
    };
    console.info('==> 🌎 Listening on port %s.', port);
  });
  server.setTimeout(config.serverTimeout);
});

const gracefulExit = () => {
  mongoose.connection.close( () => {
    logger.debug('Mongoose connection to DB is disconnected ' +
                 'through app termination');
    process.exit();
  })
}

process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);

// Main
if (!module.parent) {
  try {
    const mongodbOptions = {
      useNewUrlParser: true,
      reconnectTries: 10,
      reconnectInterval: 1000,
    };
    const promiseRetryOptions = {
      retries: mongodbOptions.reconnectTries,
      factor: 1.5,
      minTimeout: mongodbOptions.reconnectInterval,
      maxTimeout: 5000,
    }
    const {uri, username, password} = config.db;
    if (username) {
      mongodbOptions.user = username;
    };
    if (password) {
      mongodbOptions.pass = password;
    };
    mongoose.set('debug', config.mongoose.debug);
    mongoose.set('useCreateIndex', true);

    promiseRetry((retry, number) => {
      console.log(`Mongoose connecting to ${uri} - retry number: ${number}`)
      return mongoose.connect(uri, mongodbOptions).catch(retry);
    }, promiseRetryOptions)
    .catch((e)=>logger.error(e));

  } catch(err) {
    logger.error('Server initialization failed %s', err.message);
  }
}

const logErrors = (err, req, res, next) => {
  config.logger.error(err.stack);
  next(err);
}

const clientErrorHandler = (err, req, res, next) => {
  if (req.xhr) {
    if (err === 'Signature verification failed') {
      res.status(403).json({error: err});
    } else {
      res.status(500).json({error: String(err) || 'Server error'});
    };
  } else {
    next(err);
  };
}

const errorHandler = (err, req, res, next) => {
  res.status(500).json({error: String(err) || 'Server error'});
}

// Error handlers
app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);
