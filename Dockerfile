FROM node:10.16.0
WORKDIR /opt/akis
ADD package.json .
ADD yarn.lock .
RUN npm install
ADD . /opt/akis
EXPOSE 3001
ENV NODE_ENV=production PORT=3001

# Build client web app
RUN cd client && yarn install && yarn build

ENTRYPOINT node server

